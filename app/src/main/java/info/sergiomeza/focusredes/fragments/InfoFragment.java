package info.sergiomeza.focusredes.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import info.sergiomeza.focusredes.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InfoFragment extends Fragment {

    private String mInfo ;
    public static InfoFragment newInstance(String mInfo) {
        Bundle args = new Bundle();
        args.putString("Info", mInfo);
        InfoFragment fragment = new InfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInfo = getArguments().getString("Info");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_info, container, false);
        TextView mTxtInfo = (TextView)v.findViewById(R.id.mInfoText);
        mTxtInfo.setText(mInfo);
        return v;
    }
}
