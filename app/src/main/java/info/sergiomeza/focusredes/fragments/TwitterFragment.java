package info.sergiomeza.focusredes.fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import info.sergiomeza.focusredes.R;
import info.sergiomeza.focusredes.adapters.StatusAdapter;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * A simple {@link Fragment} subclass.
 */
public class TwitterFragment extends Fragment {
    private String mInfo ;
    private Twitter mTwitter;
    RecyclerView mRecycler;
    StatusAdapter mAdapter;
    SwipeRefreshLayout mSwipe;

    public static TwitterFragment newInstance(String mInfo) {
        Bundle args = new Bundle();
        args.putString("tw", mInfo);
        TwitterFragment fragment = new TwitterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInfo = getArguments().getString("tw");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new GetUserTimeLine().execute(mInfo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_twitter, container, false);
        mRecycler = (RecyclerView)v.findViewById(R.id.mRecycler);
        mSwipe = (SwipeRefreshLayout)v.findViewById(R.id.mSwipe);
        //CONFIGURACION DE TWITTER

        twitterConfig();
        if(mTwitter != null){
            //SE COMPRUEBA SI EL USUARIO ES DIFERENTE VACIO
            if(mInfo.length() == 0){
                mInfo = "focuspanama";
                Toast.makeText(getActivity(), "Mostrando Feed de Focus..", Toast.LENGTH_LONG).show();
            }

            mRecycler.setHasFixedSize(true);
            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            mRecycler.setLayoutManager(llm);
            mRecycler.setAdapter(null);
        }

        //CUANDO SE DA REFRESH
        mSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetUserTimeLine().execute(mInfo);
            }
        });

        return v;
    }

    /**
     * CONFIGURAR TWITTER
     */
    private void twitterConfig(){
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(getActivity().getString(R.string.consumer_key))
                .setOAuthConsumerSecret(getActivity().getString(R.string.consumer_secret))
                .setOAuthAccessToken(getActivity().getString(R.string.tw_access_token))
                .setOAuthAccessTokenSecret(getActivity().getString(R.string.tw_token_secret));
        TwitterFactory tf = new TwitterFactory(cb.build());
        mTwitter = tf.getInstance();
    }

    /**
     * DEVUELVE TWEETS
     */
    class GetUserTimeLine extends AsyncTask<String, Void, List<Status>>{
        @Override
        protected void onPreExecute() {
            mSwipe.post(new Runnable() {
                @Override
                public void run() {
                    mSwipe.setRefreshing(true);
                }
            });
        }

        @Override
        protected List<twitter4j.Status> doInBackground(String... strings) {
            List<twitter4j.Status> mStatuses = new ArrayList<>();
            if(mTwitter != null) {
                try {
                    //SE HACE EL REQUEST AL TIMELINE
                    mStatuses = mTwitter.getUserTimeline(strings[0]);
                } catch (Exception ignored) {
                }
            }

            return mStatuses;
        }

        @Override
        protected void onPostExecute(List<twitter4j.Status> statuses) {
            super.onPostExecute(statuses);
            mSwipe.setRefreshing(false);
            if(statuses != null){
                if(statuses.size() > 0){
                    mAdapter = new StatusAdapter(statuses);
                    mRecycler.setAdapter(mAdapter);
                }
                else {
                    Snackbar.make(mRecycler, "El Twitter del usuario no existe", Snackbar.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

        }
    }
}
