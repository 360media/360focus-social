package info.sergiomeza.focusredes.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import info.sergiomeza.focusredes.Objects.FbData;
import info.sergiomeza.focusredes.Objects.PhotosId;
import info.sergiomeza.focusredes.R;
import info.sergiomeza.focusredes.adapters.StatusAdapter;

public class FbFragment extends Fragment {
    String mLink;
    RecyclerView mRecycler;
    StatusAdapter mAdapter;
    SwipeRefreshLayout mSwipe;

    public static FbFragment newInstance(String mLink) {
        Bundle args = new Bundle();
        args.putString("fb", mLink);
        FbFragment fragment = new FbFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLink = getArguments().getString("fb");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_fb, container, false);
        mRecycler = (RecyclerView)v.findViewById(R.id.mRecycler);
        mSwipe = (SwipeRefreshLayout)v.findViewById(R.id.mSwipe);

        mRecycler.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecycler.setLayoutManager(llm);
        mRecycler.setAdapter(null);

        //CUANDO SE DA REFRESH
        mSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getFbData();
            }
        });

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getFbData();
    }

    /**
     * DEVUELVE LOS DATOS DESDE FB
     */
    private void getFbData(){
        if(AccessToken.getCurrentAccessToken() == null) {
            AccessToken mAccessToken = new AccessToken(
                    getActivity().getString(R.string.fb_access_token),
                    getActivity().getString(R.string.fb_app_id), "10153912255146444",
                    null, null, null, null, null);
            AccessToken.setCurrentAccessToken(mAccessToken);
        }

        if(mLink.length() == 0){
            mLink = "focuspanama";
            Toast.makeText(getActivity(), "Mostrando Feed de Focus..", Toast.LENGTH_LONG).show();
        }
        /* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + mLink + "/photos",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        mSwipe.setRefreshing(false);
                        if(response.getError() != null){
                            Snackbar.make(mRecycler, response.getError().toString(),
                                    Snackbar.LENGTH_LONG).show();
                            Log.d("ERRO FB", response.getError().toString());
                        }

                        else {
                            try {
                                JSONArray mData = response.getJSONObject().getJSONArray("data");
                                Type listType = new TypeToken<ArrayList<PhotosId>>(){}.getType();
                                List<PhotosId> mFbDataList = new Gson().fromJson(mData.toString(), listType);
                                if(mFbDataList.size() > 0){
                                    mAdapter = new StatusAdapter(mFbDataList);
                                    mRecycler.setAdapter(mAdapter);
                                }
                                else {
                                    Snackbar.make(mRecycler, "El Facebook del usuario no existe",
                                            Snackbar.LENGTH_LONG).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        ).executeAsync();

        mSwipe.post(new Runnable() {
            @Override
            public void run() {
                mSwipe.setRefreshing(true);
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
//            getFbData();
        }
    }

}
