package info.sergiomeza.focusredes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import info.sergiomeza.focusredes.Objects.Client;
import info.sergiomeza.focusredes.Objects.FbData;

public class InicioTestActivity extends AppCompatActivity {

    Spinner mClientSpinner;
    ArrayAdapter mSpinneradapter;
    private boolean isSpinnerTouched = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_test);

        mClientSpinner = (Spinner)findViewById(R.id.mClientSpinner);
        loadData();
    }

    /**
     * CARGAR LISTA DESDE LOCAL FILE
     */
    private void loadData(){
        final String mClients = Util.getJson(this);
        try {
            Type listType = new TypeToken<ArrayList<Client>>() {
            }.getType();
            final List<Client> mClientList = new Gson().fromJson(mClients, listType);
            mSpinneradapter = new ArrayAdapter(this,
                    R.layout.support_simple_spinner_dropdown_item, mClientList);
            mClientSpinner.setAdapter(mSpinneradapter);

            mClientSpinner.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    isSpinnerTouched = true;
                    return false;
                }
            });
            mClientSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (!isSpinnerTouched) return; else {
                        Intent mMain = new Intent(getApplicationContext(), MainActivity.class);
                        mMain.putExtra("client", new Gson().toJson(mClientList.get(i)));
                        startActivity(mMain);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
        catch (Exception ignored){}
    }
}
