package info.sergiomeza.focusredes;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Sergio Meza el 10/3/16.
 */

public class Util {

    public static String getJson(Context mContext){
        String json = null;
        try {
            InputStream is = mContext.getAssets().open("clients.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
