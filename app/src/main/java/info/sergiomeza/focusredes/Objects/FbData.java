package info.sergiomeza.focusredes.Objects;

/**
 * Created by Sergio Meza el 10/3/16.
 */

public class FbData {
    public String id;
    public String created_time;
    public String message;
    public String story;

    public FbData(){}

    public FbData(String id, String created_time, String message, String story) {
        this.id = id;
        this.created_time = created_time;
        this.message = message;
        this.story = story;
    }
}
