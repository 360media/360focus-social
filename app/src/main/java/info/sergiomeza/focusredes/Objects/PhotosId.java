package info.sergiomeza.focusredes.Objects;

/**
 * Created by Sergio Meza el 10/10/16.
 */

public class PhotosId {
    public String id;
    public String created_time;

    public PhotosId(String id, String created_time) {
        this.id = id;
        this.created_time = created_time;
    }
}
