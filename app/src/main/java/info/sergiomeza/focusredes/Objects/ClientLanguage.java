
package info.sergiomeza.focusredes.Objects;

import java.util.HashMap;
import java.util.Map;

public class ClientLanguage {

    private Integer id;
    private Integer languagesId;
    private Integer clientsId;
    private String description;
    private Language language;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The languagesId
     */
    public Integer getLanguagesId() {
        return languagesId;
    }

    /**
     * 
     * @param languagesId
     *     The languages_id
     */
    public void setLanguagesId(Integer languagesId) {
        this.languagesId = languagesId;
    }

    /**
     * 
     * @return
     *     The clientsId
     */
    public Integer getClientsId() {
        return clientsId;
    }

    /**
     * 
     * @param clientsId
     *     The clients_id
     */
    public void setClientsId(Integer clientsId) {
        this.clientsId = clientsId;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The language
     */
    public Language getLanguage() {
        return language;
    }

    /**
     * 
     * @param language
     *     The language
     */
    public void setLanguage(Language language) {
        this.language = language;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
