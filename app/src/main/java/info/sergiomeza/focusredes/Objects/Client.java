
package info.sergiomeza.focusredes.Objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Client {

    private Integer id;
    private String name;
    private String image;
    private String status;
    private Integer onDiscover;
    private String created;
    private List<ClientLanguage> client_languages = new ArrayList<ClientLanguage>();
    private List<ClientCategory> clientCategories = new ArrayList<ClientCategory>();
    private List<ClientSocial> client_social = new ArrayList<ClientSocial>();
    private List<ClientLocation> clientLocations = new ArrayList<ClientLocation>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @Override
    public String toString() {
        return name;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The image
     */
    public String getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The onDiscover
     */
    public Integer getOnDiscover() {
        return onDiscover;
    }

    /**
     * 
     * @param onDiscover
     *     The on_discover
     */
    public void setOnDiscover(Integer onDiscover) {
        this.onDiscover = onDiscover;
    }

    /**
     * 
     * @return
     *     The created
     */
    public String getCreated() {
        return created;
    }

    /**
     * 
     * @param created
     *     The created
     */
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * 
     * @return
     *     The client_languages
     */
    public List<ClientLanguage> getClient_languages() {
        return client_languages;
    }

    /**
     * 
     * @param client_languages
     *     The client_languages
     */
    public void setClient_languages(List<ClientLanguage> client_languages) {
        this.client_languages = client_languages;
    }

    /**
     * 
     * @return
     *     The clientCategories
     */
    public List<ClientCategory> getClientCategories() {
        return clientCategories;
    }

    /**
     * 
     * @param clientCategories
     *     The client_categories
     */
    public void setClientCategories(List<ClientCategory> clientCategories) {
        this.clientCategories = clientCategories;
    }

    /**
     * 
     * @return
     *     The client_social
     */
    public List<ClientSocial> getClient_social() {
        return client_social;
    }

    /**
     * 
     * @param client_social
     *     The client_social
     */
    public void setClient_social(List<ClientSocial> client_social) {
        this.client_social = client_social;
    }

    /**
     * 
     * @return
     *     The clientLocations
     */
    public List<ClientLocation> getClientLocations() {
        return clientLocations;
    }

    /**
     * 
     * @param clientLocations
     *     The client_locations
     */
    public void setClientLocations(List<ClientLocation> clientLocations) {
        this.clientLocations = clientLocations;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
