
package info.sergiomeza.focusredes.Objects;

import java.util.HashMap;
import java.util.Map;

public class ClientSocial {

    private Integer id;
    private Integer socialId;
    private Integer clientsId;
    private String url;
    private Social social;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The socialId
     */
    public Integer getSocialId() {
        return socialId;
    }

    /**
     * 
     * @param socialId
     *     The social_id
     */
    public void setSocialId(Integer socialId) {
        this.socialId = socialId;
    }

    /**
     * 
     * @return
     *     The clientsId
     */
    public Integer getClientsId() {
        return clientsId;
    }

    /**
     * 
     * @param clientsId
     *     The clients_id
     */
    public void setClientsId(Integer clientsId) {
        this.clientsId = clientsId;
    }

    /**
     * 
     * @return
     *     The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param url
     *     The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 
     * @return
     *     The social
     */
    public Social getSocial() {
        return social;
    }

    /**
     * 
     * @param social
     *     The social
     */
    public void setSocial(Social social) {
        this.social = social;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
