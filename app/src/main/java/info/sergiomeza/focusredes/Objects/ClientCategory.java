
package info.sergiomeza.focusredes.Objects;

import java.util.HashMap;
import java.util.Map;

public class ClientCategory {

    private Integer id;
    private Integer clientsId;
    private Integer categoriesId;
    private Category category;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The clientsId
     */
    public Integer getClientsId() {
        return clientsId;
    }

    /**
     * 
     * @param clientsId
     *     The clients_id
     */
    public void setClientsId(Integer clientsId) {
        this.clientsId = clientsId;
    }

    /**
     * 
     * @return
     *     The categoriesId
     */
    public Integer getCategoriesId() {
        return categoriesId;
    }

    /**
     * 
     * @param categoriesId
     *     The categories_id
     */
    public void setCategoriesId(Integer categoriesId) {
        this.categoriesId = categoriesId;
    }

    /**
     * 
     * @return
     *     The category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * 
     * @param category
     *     The category
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
