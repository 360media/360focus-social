
package info.sergiomeza.focusredes.Objects;

import java.util.HashMap;
import java.util.Map;

public class ClientLocation {

    private Integer id;
    private Integer clientsId;
    private Integer provincesId;
    private String phone;
    private String email;
    private String address;
    private String latitude;
    private String longitude;
    private String schedule;
    private Province province;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The clientsId
     */
    public Integer getClientsId() {
        return clientsId;
    }

    /**
     * 
     * @param clientsId
     *     The clients_id
     */
    public void setClientsId(Integer clientsId) {
        this.clientsId = clientsId;
    }

    /**
     * 
     * @return
     *     The provincesId
     */
    public Integer getProvincesId() {
        return provincesId;
    }

    /**
     * 
     * @param provincesId
     *     The provinces_id
     */
    public void setProvincesId(Integer provincesId) {
        this.provincesId = provincesId;
    }

    /**
     * 
     * @return
     *     The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 
     * @param phone
     *     The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * 
     * @param address
     *     The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 
     * @return
     *     The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * 
     * @param latitude
     *     The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * 
     * @return
     *     The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * 
     * @param longitude
     *     The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * 
     * @return
     *     The schedule
     */
    public String getSchedule() {
        return schedule;
    }

    /**
     * 
     * @param schedule
     *     The schedule
     */
    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    /**
     * 
     * @return
     *     The province
     */
    public Province getProvince() {
        return province;
    }

    /**
     * 
     * @param province
     *     The province
     */
    public void setProvince(Province province) {
        this.province = province;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
