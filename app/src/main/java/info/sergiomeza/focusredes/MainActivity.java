package info.sergiomeza.focusredes;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import info.sergiomeza.focusredes.Objects.Client;
import info.sergiomeza.focusredes.adapters.TabsAdapter;

public class MainActivity extends AppCompatActivity {
    ViewPager mViewPager;
    TabsAdapter mTabsAdapter;
    TabLayout mTabLayout;
    ImageView mImage;
    private CollapsingToolbarLayout mCollabLayout;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mImage = (ImageView) findViewById(R.id.mImageClient);
        mCollabLayout = (CollapsingToolbarLayout) findViewById(R.id.coll_toolbar);
        mViewPager = (ViewPager)findViewById(R.id.mViewPager);
        mTabLayout = (TabLayout)findViewById(R.id.mTabLayout);
        initActivity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    /**
     * INICIAR ACTIVIDAD
     */
    private void initActivity(){
        //DUMMY CLIENT
        if(getIntent().hasExtra("client")) {
            Client mClient = new Gson().fromJson(getIntent().getExtras().getString("client"), Client.class);
            mCollabLayout.setTitle(mClient.getName());
            //CARGAR IMAGEN
            Picasso.with(this)
                    .load("http://104.236.60.211/360focus/img/" + mClient.getImage())
                    .into(mImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap bitmap = ((BitmapDrawable) mImage.getDrawable()).getBitmap();
                            Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                                @Override
                                public void onGenerated(Palette palette) {
                                    try {
                                        Palette.Swatch muted = palette.getDarkMutedSwatch();
                                        mCollabLayout.setContentScrimColor(muted.getRgb());
                                    }
                                    catch (Exception ignored){}
                                }
                            });
                        }

                        @Override
                        public void onError() {
                        }
                    });

            //INICIALIZAR TABS
            mTabLayout.addTab(mTabLayout.newTab().setText(R.string.info));
            mTabLayout.addTab(mTabLayout.newTab().setText(R.string.facebook));
            mTabLayout.addTab(mTabLayout.newTab().setText(R.string.twitter));
            mTabLayout.addTab(mTabLayout.newTab().setText(R.string.instagram));

            mTabsAdapter = new TabsAdapter(this.getSupportFragmentManager(), mTabLayout.getTabCount(), mClient);
            mViewPager.setAdapter(mTabsAdapter);
            mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

            mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    mViewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

            this.setTitle(mClient.getName());
            mCollabLayout.setCollapsedTitleTextAppearance(R.style.CollapsingToolbarLayoutExpandedTextStyle);
            toolbar.setTitle(mClient.getName());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
