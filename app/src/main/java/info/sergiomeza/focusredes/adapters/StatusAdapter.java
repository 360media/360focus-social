package info.sergiomeza.focusredes.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import info.sergiomeza.focusredes.Objects.FbData;
import info.sergiomeza.focusredes.Objects.PhotosId;
import info.sergiomeza.focusredes.R;
import twitter4j.Status;

/**
 * Created by Sergio Meza el 10/3/16.
 */

public class StatusAdapter
        extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<?> mStatus = new ArrayList<>();
    private static final int IMAGE_TYPE = 0;
    private static final int TEXT_TYPE = 1;

    public StatusAdapter(List<?> mStatus) {
        this.mStatus = mStatus;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if(viewType == IMAGE_TYPE){
            itemView = LayoutInflater.
                    from(parent.getContext()).
                    inflate(R.layout.listitem_image, parent, false);
            return new ImageHolder(itemView);
        }
        else {
            itemView = LayoutInflater.
                    from(parent.getContext()).
                    inflate(R.layout.listitem_twitter, parent, false);
            return new TextHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Object mStatus = this.mStatus.get(position);
        switch (holder.getItemViewType()){
            case IMAGE_TYPE:
                ((ImageHolder)holder).bindView((PhotosId)mStatus);
                break;

            case TEXT_TYPE:
                ((TextHolder)holder).bindView(mStatus);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        final Object mStatus = this.mStatus.get(position);
        if(mStatus instanceof PhotosId) {
            return IMAGE_TYPE;
        }
        else {
            return TEXT_TYPE;
        }
    }

    @Override
    public int getItemCount() {
        return this.mStatus.size();
    }

    /**
     * TEXTHOLDER
     */
    public class TextHolder extends RecyclerView.ViewHolder {
        TextView mStatus, mFecha;

        public TextHolder(View itemView) {
            super(itemView);
            this.mFecha = (TextView)itemView.findViewById(R.id.mTwFecha);
            this.mStatus = (TextView)itemView.findViewById(R.id.mTwStatus);
        }

        public void bindView(Object mStatus){
            if(mStatus instanceof Status) {
                this.mStatus.setText(((Status) mStatus).getText());
                this.mFecha.setText(new Gson().toJson(((Status) mStatus).getCreatedAt()).toString());
            }

//            if(mStatus instanceof FbData) {
//                if(((FbData) mStatus).message != null) {
//                    this.mStatus.setText(((FbData) mStatus).message);
//                }
//                else {
//                    this.mStatus.setText(((FbData) mStatus).story);
//                }
//                this.mFecha.setText(((FbData) mStatus).created_time);
//            }
        }
    }

    public class ImageHolder extends RecyclerView.ViewHolder {
        ImageView mImage;
        TextView mTitle;

        public ImageHolder(View itemView) {
            super(itemView);
            this.mImage = (ImageView) itemView.findViewById(R.id.mImageItem);
            this.mTitle = (TextView) itemView.findViewById(R.id.mTxtTitleItem);
        }

        public void bindView(PhotosId mPhoto) {
            new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + mPhoto.id + "?fields=images,likes",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            JSONObject mImageObj = new JSONObject(response.getJSONObject()
                                    .getJSONArray("images").get(0).toString());
                            Picasso.with(mImage.getContext())
                                    .load(mImageObj.get("source").toString())
                                    .fit()
                                    .into(mImage);
                            if(!response.getJSONObject().isNull("likes")) {
                                mTitle.setText(response.getJSONObject().getJSONObject("likes")
                                        .getJSONArray("data").length() + " likes");
                            }
                            else {
                                mTitle.setText("0 likes");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).executeAsync();
        }
    }
}
