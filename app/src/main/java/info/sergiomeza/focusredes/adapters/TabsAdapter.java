package info.sergiomeza.focusredes.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import info.sergiomeza.focusredes.Objects.Client;
import info.sergiomeza.focusredes.fragments.FbFragment;
import info.sergiomeza.focusredes.fragments.InfoFragment;
import info.sergiomeza.focusredes.fragments.InstaFragment;
import info.sergiomeza.focusredes.fragments.TwitterFragment;

/**
 * Created by Sergio Meza el 10/3/16.
 */

public class TabsAdapter extends FragmentStatePagerAdapter {
    int mCount;
    Client mClient;
    public TabsAdapter(FragmentManager fm, int mCount, Client mClient) {
        super(fm);
        this.mCount = mCount;
        this.mClient = mClient;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return InfoFragment.newInstance(mClient.getClient_languages().get(0).getDescription());

            case 1:
                return FbFragment.newInstance(mClient.getClient_social().get(0).getUrl());

            case 2:
                return TwitterFragment.newInstance(mClient.getClient_social().get(1).getUrl());

            case 3:
                return new InstaFragment();

            default:
                return null;
        }
    }


    @Override
    public int getCount() {
        return this.mCount;
    }
}
